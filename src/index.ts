const age: number = 113;

const obj = {
  one: {
    two: {
      three: "nested string",
    },
  },
};

function log(input: any) {
  // if input = null
  console.log(input?.one?.two?.three); // undefined
  console.log(input && input.one && input.two && input.three); // input
}

log(0);
