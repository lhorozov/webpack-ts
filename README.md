## Initial project Webpack setup for TypeScript

Open your projects folder and follow these CLI commands:
```
	mkdir webpack-ts
	cd webpack-ts
	npm init -y
	npm i -D webpack
	touch index.js
	npx webpack
	npm i -D ts-loader
	npm i -D typescript
	npx tsc --init
	npm i -D cross-env
```
